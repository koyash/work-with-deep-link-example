import logo from './assets/logo.svg';
import './App.css';
import {useLocation} from "react-router-dom";
import {useEffect} from "react";

function App() {
    const search = useLocation().search;
    const redirectPath = new URLSearchParams(search).get('redirect_path');
    const resultPath = 'folk-app://app.example.com/' + redirectPath;
    const androidLike = 'Linux OS';
    const iOSLike = 'UNIX OS';
    console.log('resultPath = ' + resultPath);
    function getOperatingSystem(window) {
        let operatingSystem = 'Not known';
        if (window.navigator.appVersion.indexOf('Win') !== -1) { operatingSystem = 'Windows OS'; }
        if (window.navigator.appVersion.indexOf('Mac') !== -1) { operatingSystem = 'MacOS'; }
        if (window.navigator.appVersion.indexOf('X11') !== -1) { operatingSystem = iOSLike; }
        if (window.navigator.appVersion.indexOf('Linux') !== -1) { operatingSystem = androidLike; }

        return operatingSystem;
    }
    const OS = getOperatingSystem(window);

    useEffect(() => {
        if (OS === androidLike || OS === iOSLike) {
            window.location.replace(resultPath);
        }
    }, []);
    function handleLogoClick() {
        alert('Типо откроется веб версия приложения');
    }

    return (
        <div className='App'>
            <header>
                <img src={logo} onClick={handleLogoClick} alt=''/>
            </header>
            <div>
                <p>Вы пытаетесь открыть в мобильном приложении {redirectPath}</p>
                <p>Если открытие не произошло автоматически то можете нажать на <a href={resultPath}>ссылку</a></p>
                <p>Ещё нет приложения? СКАЧАЙ!</p>
            </div>
        </div>
    );
}

export default App;
